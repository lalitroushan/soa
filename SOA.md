# SERVICE ORIENTED ARCHITECTURE (SOA)


## Definition -

### SOA is a way of making software components reusable through interfaces.The interfaces use common communication standards so that they can be incorporated into new apps without performing deep integration each time.


### In simple words, the entire software application is not built as one but the different features and services are broken down into smaller components, that is why it is also called "Coarse-Grained Architecture".

<br>

![](https://miro.medium.com/max/1400/1*Xot9nbkQAGbGaYwi84Kh-w.png)

---
<br>

## Example -
![img](https://miro.medium.com/max/2400/1*QdrmRfhGbaj7zotALKNyIQ.png)

### In the diagram of SOA, it is shown four different circles. All those four circles are representing 4 different services that have their own features or multiple tasks inside them. All the tasks combine and deliver as one particular feature. 

---

## Types of SOA -

|Business Service |Enterprise Service |Application Service |Infrastructure Service |
|--- |--- |--- |--- |
|Business Users |Shared Services Team |Application Development Team |Infrastructure Service Team|





>Business Services
---
This service performs core business operations and it can be either represented by your XML or web service definition language.
<br>

>Enterprise Services

This service implements the functionality defined by your business service and it does with the help of the Application service and Infrastructure service.
<br>

>Application Service

This is the actual core application or core functionality of any particular feature. These Application services can be invoked directly or through a user interface.
<br>

>Infrastructure Service

This service refers to non-business or non-technical operations such as auditing, scaling or security.

--- 

## Heterogeneous Interoperability

### In the main software application, there might have smaller applications, each of those applications could be written in any language, when it is written in different programming languages it's tough for them to interact with one another. To sort these complications we have a platform called "Messaging Middleware".


### "Messaging Middleware"  acts as an interaction point between the different applications which are in a different language.

<br>

![](https://docs.oracle.com/cd/E19340-01/820-6424/images/to_middleware.gif)

 <br>

 ---
 ## Service Granularity

 ### In SOA the Service Consumer would again be performing one particular operation. The operation would be calling the different services all at the same time so the operation would be calling service 1, service 2, service 3... so on and all of them will be encompassed all together.

 ### So, by calling one service all sub-services would be called at the same time. And this could help in saving time.

---
## Why Service-Oriented Architecture Is Important

- SOA can be used to create reusable code.
- It promotes interaction with various services and platforms.
- It provides great scalability as it cuts back on the client-service interaction.
- For cost reduction, it is also very helpful as it limits the amount of analysis required when deploying custom solutions.
<br>

![](https://miro.medium.com/max/610/1*QpkU690MioKK7lHwLE0_Bg.png)

***

## References -

- [About SOA](https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec)
- [Distributed Architectures](https://www.edureka.co/blog/microservices-vs-soa/)
